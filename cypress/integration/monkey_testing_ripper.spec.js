describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(10);
    })
})
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};
function randomClick(monkeysLeft) {
    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({force: true});
                monkeysLeft = monkeysLeft - 1;
            }
            cy.wait(1000);
            randomClick(monkeysLeft);
        });
    }   
}
function randomEvent(monkeysLeft) {
    var validElements = ['a', 'input[type=text]', '[type="checkbox"]', 'button']
    if (monkeysLeft > 0) {
        var element  = validElements[getRandomInt(0, validElements.length)]
        var domElement = Cypress.$(element)
        if (domElement.length > 0) {
            cy.get(element).then($elements => {
                var randomElement = $elements.get(getRandomInt(0, $elements.length));
                if(!Cypress.dom.isHidden(randomElement)) {
                    var wrappedElement = cy.wrap(randomElement)
                    switch (element) {
                        case 'a':
                            wrappedElement.click({ force: true });
                            break;
                        case 'input[type=text]':
                            wrappedElement.type('randomText', { force: true });
                            break;
                        case '[type="checkbox"]':
                            wrappedElement.check({ force: true });
                            break;
                        case 'button':
                            wrappedElement.click({ force: true });
                            break;
                        default:
                            break;
                    }
                    monkeysLeft = monkeysLeft-1
                }
                cy.wait(1000);
                randomEvent(monkeysLeft);
            })
        } else {
            randomEvent(monkeysLeft)
        }
    }
}